const express = require('express'),
      app = express(),
      server=require('http').createServer(app),
      io = require('socket.io')(server),
      FileSync = require('lowdb/adapters/FileSync'),
      db = require('lowdb')(new FileSync('db.json')),
      multer = require('multer'),
      upload = multer(),
      Joi = require('@hapi/joi'),
      bcrypt = require('bcrypt'),
      jwt = require('jsonwebtoken'),
      cookieParser = require('cookie-parser'),
      sanitizeHTML = require('sanitize-html'),
      compression = require('compression'),
      ioCookieParser = require('socket.io-cookie');

require('dotenv').config();
const port = 8000;
server.listen(port, ()=>{
    console.log(`listening on port ${port}`); 
});
app.use(compression());
app.use(express.static('public'));
app.use(cookieParser());
app.post('/register', [upload.none() , validate], async (req, res)=>{
  console.log(req.body);
  let {name, username, password} = req.body;
  password = await bcrypt.hash(password, 10);
  await db.get('users').push({name,username,password,rooms:[]}).write();
  res.cookie('access_token',jwt.sign({username}, process.env.ACCESS_TOKEN_SECRET),{maxAge: 2592000000, httpOnly: true})
  .cookie('data', JSON.stringify({name,un: username}),{maxAge: 2592000000})
  .status(201).send();
});
app.post('/signin', upload.none(), async (req, res)=>{
  console.log(req.body);
  const {username,password} = req.body;
  const user = db.get('users').find({username}).value();
  if (user == undefined) {
    res.status(401).send({error: "username was not found"});
    return
  }
  const match = await bcrypt.compare(password, user.password);
  if (!match) {
    res.status(401).send({error: "password doesn't match the given username"});
    return
  }
  const access_token = jwt.sign({id: user.id,username}, process.env.ACCESS_TOKEN_SECRET);

  res.cookie('access_token', access_token,{maxAge: 2592000000, httpOnly:true})
  .cookie('data', JSON.stringify({name: user.name,un: username}),{maxAge: 2592000000}).status(200).send();
})
app.use(express.json())
function notAuthorized() {
  res.status(401).send('Not Valid Token')
}

app.post('/createchannel', (req, res)=>{
  jwt.verify(req.cookies.access_token, process.env.ACCESS_TOKEN_SECRET,(err,payload)=>{
    if(err) {notAuthorized(); return}
    while(true){
      const id = getshortid();
      const room = db.get('rooms').find({id}).value();
      if (room== undefined) {
        db.get('rooms')
        .push({id, name: sanitizeHTML(req.body.name), members: {[payload.username]:{admin:true}}, messages:[]}).write();
        db.get('users').find({username: payload.username}).get('rooms').push(id).write();
        res.status(201).send({roomid: id});
        return;
      }
    }
  })
})
app.post('/namechange', (req, res)=>{
  jwt.verify(req.cookies.access_token, process.env.ACCESS_TOKEN_SECRET, (err, payload)=>{
    if (err) {notAuthorized();return}
    const name = sanitizeHTML(req.body.name);
    db.get('users').find({username: payload.username}).set('name', name).write();
    res.cookie('data', JSON.stringify({name,un: payload.username}), {maxAge: 2592000000}).status(200).send();
  })
})
app.post('/clear', (req,res)=>{
  res.cookie('access_token', '', {maxAge: 0})
  .cookie('data', '', {maxAge: 0}).send();
})
app.get('/channeldata',(req, res)=>{
  jwt.verify(req.cookies.access_token, process.env.ACCESS_TOKEN_SECRET, (err, payload)=>{
    if (err) {notAuthorized();return}
    console.log(req.query.cid);
    const isRoomMember = db.get('users').find({username: payload.username}).get('rooms').value().indexOf(req.query.cid)!=-1;//.has(req.query.cid).value();
    console.log(isRoomMember);
    if(isRoomMember){
      res.send(db.get('rooms').find({id: req.query.cid}).get('members').value())
    }else{
      res.status(404).send('error channel not found');
    }
    //
  })
})
function getshortid() {
  let chars='abcdefghigklmnopqrstuvwxyz123456789';
  chars=chars.split('');
  let id='';
  for (let i = 0; i < 4; i++) {
    let index = Math.floor(Math.random()*chars.length);
    if (index>=chars.length) index -= 1;
    id += chars[index];   
  }
  return id;
}
async function validate(req, res, next) {
  const {name, username, password} = req.body;
  const schema = Joi.object({
    name: Joi.string().min(1).max(20).required(),
    username: Joi.string().min(1).max(12).required(),
    password: Joi.string().min(8).max(20).required()
  })
  try{
    await schema.validateAsync({name,username,password});
  }catch(err){
    err._original.password = undefined;
    res.status(400).json({error:err.details[0].message});
    return;
  }
  const dbusername = db.get('users').find({username}).value();
  if (dbusername != undefined) {
    res.status(400).send({error: 'Already taken username'});
    return
  }
  else{
    next();
  }
}


db.defaults({users:[], rooms: []}).write();
db._.mixin({
  del: function(obj, toDelete) {
    if (Array.isArray(obj)) {
      if(obj.indexOf(toDelete)>-1){
        obj.splice(obj.indexOf(toDelete), 1);
      }
    }else{
      delete obj[toDelete];
    }
    
    return obj;
  }
})
let connumber=0;
io.use(ioCookieParser);
let usersSocketIds = {};
io.on('connection', socket=>{
    console.log(`socket connected with id ${socket.id}`);
    const cookie =  jwt.verify(socket.handshake.headers.cookie.access_token, process.env.ACCESS_TOKEN_SECRET,(err,payload)=>{
      if (err) {
        socket.emit('err',err);
        socket.disconnect(true);
        return false;  
      }
      return payload;
    });
    if (cookie === false) return;
    if (usersSocketIds[cookie.username]) {
      usersSocketIds[cookie.username].push(socket.id);
    }else{
      usersSocketIds[cookie.username] = [socket.id];
    }
    
    //console.log(usersSocketIds);
    //console.log(Object.keys(io.sockets.connected).length);
    const rooms = db.get('users').find({username: cookie.username}).get('rooms').value();
    
    //socket.handshake.headers.referer
    if(rooms == undefined){socket.emit('err','profile not found');socket.disconnect(true);return}
    let roomsdata;
    if(rooms.length>0) roomsdata = db.get('rooms').value();
    let messages = [];
    for (let i = 0; i < rooms.length; i++) {
      const room = rooms[i];
      const data = roomsdata.find(roomdata=>roomdata.id == room);
      socket.join(room);
      messages.push({
        id:room,
        name: data.name,
        messages: data.messages
      })
    }
    //console.log(io.sockets.adapter.rooms['hgi5']);
    socket.emit('init', messages);

    function requireAdministration(channel,callback) {
      const isAdmin = db.get('rooms').find({id: channel}).get('members').get(cookie.username).get('admin').value();
      if (isAdmin) {
        callback();
      }else{
        socket.emit('ordErr', {...data, err: 'Not an admin of this group'});
      }
    }

    socket.on('chat', (data)=>{
        data.message = Buffer.from(data.message, 'base64').toString('utf-8');
        data.message = Buffer.from(sanitizeHTML(data.message), 'utf-8').toString('base64');
        data.un = cookie.username;
        console.log(data.channel);
        if(rooms.indexOf(data.channel) != -1){
          db.get('rooms').find({id: data.channel}).get('messages').push(data).write();
          io.to(data.channel).emit('chat', data);
        } 
    })
    socket.on('typing', (data)=>{
        socket.to(data.channel).broadcast.emit('typing',data);
    })
    socket.on('ord', (data)=>{
      const message = Buffer.from(data.message, 'base64').toString('utf-8');
      const regex = /^!\/(\w+)(?:[\s](\w+))?$/g;
      if (message.search(regex) != -1) {    
        const match = regex.exec(message);
        switch (match[1]) {
          case 'FR':
            io.to(data.channel).emit('clearChat',{channel: data.channel});
            break;
          case 'FRA':
            io.emit('clearChat');
            break;
          case 'LOCK':
            if(match[2]== 'this' && rooms.indexOf(data.channel)>-1){
              requireAdministration(data.channel,()=>{
                db.get('rooms').find({id: data.channel}).set('locked', true).write();
                io.to(data.channel).emit('lockChange', {locked: true, un: cookie.username ,channel: data.channel})
              })
            }else{
              socket.emit('ordErr', data);
            }
            break;
          case 'UNLOCK':
            if(match[2]== 'this' && rooms.indexOf(data.channel)>-1){
              requireAdministration(data.channel,()=> {
                db.get('rooms').find({id: data.channel}).set('locked', false).write();
                io.to(data.channel).emit('lockChange', {locked: false, un: cookie.username ,channel: data.channel})
              })
            }else{
              socket.emit('ordErr', data);
            }
            break;
          case 'KICK':
            requireAdministration(data.channel,()=> {
              const usid = usersSocketIds[match[2]];
              console.log(usid);
              data.un = cookie.username;
              usid && usid.forEach(socketId => {
                const currentSocket = io.sockets.connected[socketId];
                currentSocket.emit('clearChat', {channel: data.channel});
                currentSocket.emit('typing',{typing: false, channel: data.channel});
              });
              io.to(data.channel).emit('removed', {...data,toUn:match[2]});
              usid && usid.forEach(socketId => {
                io.sockets.connected[socketId].leave(data.channel);
              });
              db.get('users').find({username: match[2]}).get('rooms').del(data.channel).write();
              db.get('rooms').find({id: data.channel}).get('members').del(match[2]).write(); 
            });
            break;
          case 'ADMINIZE':
            requireAdministration(data.channel,()=>{
              const memberQuery = db.get('rooms').find({id: data.channel}).get(`members.${match[2]}`);
              const member = memberQuery.value();
              if (member) {
                if(!member.admin){
                  memberQuery.set('admin', true).write();
                  data.un = cookie.username;
                  io.to(data.channel).emit('adminChange',{...data, type:'added', toUn:match[2]})
                }else{
                  socket.emit('adminChange',{channel: data.channel,type:'no change'});
                }
              }
              
            });
            break;
          case 'DEADMINIZE':
            requireAdministration(data.channel,()=>{
              const memberQuery = db.get('rooms').find({id: data.channel}).get(`members.${match[2]}`);
              const member = memberQuery.value();
              if (member) {
                if(member.admin){
                  memberQuery.set('admin', false).write();
                  data.un = cookie.username;
                  io.to(data.channel).emit('adminChange',{...data, type:'removed', toUn:match[2]})
                }else{
                  socket.emit('adminChange',{channel: data.channel, type:'no change'});
                }
              }  
            });
            break;
          default:
            socket.emit('ordErr', {...data, err: 'Unknown command'});
            break;
        }
      }else{
        console.log('nah')
      }
      //console.log(data);
      /*if (data == 'تل2') {
        socket.leave(room);
        room = 'room2';
        socket.join(room);
        console.log(`${socket.id} joined g2`);
      }*/

    })
    socket.on('joinroom', room=>{
      console.log('newjoin');
      const roomid = room.id;
      console.log(roomid);
      room = db.get('rooms').find({id: roomid});
      if (!room.get('members').get(cookie.username).value()) {
        if(!(room.value()).locked){
          room.get('members').set(cookie.username,{admin:false}).write();
          db.get('users').find({username: cookie.username}).get('rooms').push(roomid).write();
          io.to(roomid).emit('join', {channel: roomid, un: cookie.username});
          socket.emit('reconnect', true);
        }else{
          socket.emit('joinErr', roomid);
        }
        
      }
      
    })
    socket.on('leave', (data)=>{
      socket.leave(data.cid);
      const index = rooms.indexOf(data.cid);
      if (index>-1){
        console.log(rooms.splice(index,1));
      }
      db.get('users').find({username: cookie.username}).get('rooms').write();
      db.get('rooms').find({id: data.cid}).get('members').del(cookie.username).write();
      io.to(data.cid).emit('leave', {channel: data.cid, un: cookie.username});
      socket.emit('reconnect', true);
    })
    socket.on('disconnect', ()=>{
      if(usersSocketIds[cookie.username]){
        const sid = usersSocketIds[cookie.username];
        const index = sid.indexOf(socket.id);
        sid.splice(index,1);
        if (sid.length == 0) {
          delete usersSocketIds[cookie.username]
        }
        console.log(usersSocketIds[cookie.username]);
      }
    })
})
