/**
 * @class
 * @param {number} maxAge 
 */
function Cache(maxAge) {
    this.storage = {}
    this.flush = (key) => {
      delete this.storage[key];
    }
    this.store = (key,value)=>{
      this.storage[key] = {
        value: value,
        expires: new Date(Date.now() + maxAge)
      }
    }
    /**
     * gets the data from the api if not cached
     * @param {string} key 
     * @param {string} path 
     * @param {object} params 
     * @returns {array}
     */
    this.get = async (key, path, params) =>{
      if(this.storage[key] && this.storage[key].expires>Date.now()){
        console.log('cached');
        return Promise.resolve(this.storage[key].value); 
      }
      else{
        return axios.get(path, {params}).then(res=>{
          this.store(key,res.data);
          console.log('live');
          return res.data;
        })
        .catch(err=>{
          console.log(err);
          return false;
        });
      }
    }
}
var showcase = document.getElementById('showcase');
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function clearcookies() {
  axios.post('/clear')
      .then(()=>{
        location.reload();
  })
}
function toggleforms(e) {
    
  switch (e.target.id) {
    case 'showreg':
      signform.style.display = "none";
      regform.style.display = "block";
      regbtn.classList.add('activetab');
      signbtn.classList.remove('activetab');
      break;
    case 'showsignin':
      signform.style.display = "block";
      regform.style.display = "none";
      signbtn.classList.add('activetab');
      regbtn.classList.remove('activetab');
      break;
    case 'showjoinchannel':
      joinform.style.display = "block";
      newform.style.display = "none";
      joinbtn.classList.add('activetab');
      newbtn.classList.remove('activetab');
      break;
    case 'shownewchannel':
      joinform.style.display = "none";
      newform.style.display = "block";
      newbtn.classList.add('activetab');
      joinbtn.classList.remove('activetab');
      break;
  }
}
const joinbtn = document.getElementById('showjoinchannel');
const newbtn = document.getElementById('shownewchannel');
const joinform = document.getElementById('joinchannel');
const newform = document.getElementById('newchannel');
const sidemenu = document.getElementById('sidemenu');
const channeldiv = document.getElementById('addchannel');
const settings = document.getElementById('settings');
const namechangeform = document.getElementById('namechangeform');
const membersdiv = document.getElementById('membersdiv');
const memberslist = document.getElementById('members');
const sideSections = document.getElementsByClassName('sidemenu-section');
newbtn.addEventListener('click', toggleforms);
joinbtn.addEventListener('click', toggleforms);
function hideAll(elemArray) {
  for (let i = 0; i < elemArray.length; i++) {
    elemArray[i].style.display = 'none'; 
  }
}
document.getElementById('showaddchannel').addEventListener('click', ()=>{
  hideAll(sideSections);
  channeldiv.style.display = 'block';
  sidemenu.style.display='block';
});
document.getElementById('showsettings').addEventListener('click', ()=>{
  hideAll(sideSections);
  settings.style.display = 'block';
  sidemenu.style.display='block';
});

const closeBtns = document.getElementsByClassName('sidemenu-close');
for (let i = 0; i < closeBtns.length; i++) {
  const closeBtn = closeBtns[i];
  closeBtn.addEventListener('click', ()=>{
    sidemenu.style.display='none';
  })
}
newform.addEventListener('submit', e=>{
  e.preventDefault();
  document.getElementById('create').disabled = true;
  axios.post('/createchannel',{name: document.getElementById('newchannelname').value}, {validateStatus: (status)=>status<500})
  .then((res)=>{
    document.getElementById('create').disabled = false;
    if (res.status == 401) {
      clearcookies();
    }else{
      location.reload();
    }
  }).catch(err=>{
    document.getElementById('create').disabled = false;
    alert(err);
  })
})
namechangeform.addEventListener('submit', e=>{
  e.preventDefault();
  const name = document.getElementById('changename').value;
  if (name != getCookie('name')) {
    axios.post('/namechange', {name}, {validateStatus: (status)=>status < 500})
    .then(res=>{
      if (res.status == 200) {
        sidemenu.style.display='none';
      }else if (res.status == 401) {
        if (confirm('Authentication failed, do you want to re login?')) {
          clearcookies();
        }
      }{

      }
    })
  }else{
    sidemenu.style.display='none';
  }
  

})
document.getElementById('logout').addEventListener('click', ()=>{
  if(confirm('Are you sure you want to Log out?')){
    clearcookies()
  }
});
if (!getCookie('data')){
  var signbtn = document.getElementById('showsignin');
  var regbtn= document.getElementById('showreg');

  var signform = document.getElementById('signin');
  var regform = document.getElementById('register');

  signbtn.addEventListener('click', toggleforms);
  regbtn.addEventListener('click', toggleforms);

  function togglepass(e) {
    var inputid = (e.currentTarget.id == 'signin-passtoggle')? 'signin-pass':'reg-pass';
    var input = document.getElementById(inputid);
    input.type = (input.type == 'text')?'password':'text';
  }
  document.getElementById('signin-passtoggle').addEventListener('click', togglepass);
  document.getElementById('reg-passtoggle').addEventListener('click', togglepass);

  function handleforms(e) {
    e.preventDefault();
    var form = e.target;
    document.getElementById(`error-${form.id}`).textContent = '';
    axios.post(form.id, new FormData(form),{validateStatus: (status)=>status < 500})
    .then((res)=>{
      if (400<=res.status && res.status<500) {
        console.log(res.data);
        document.getElementById(`error-${form.id}`).textContent = `Error: ${res.data.error}`;
      }else if(res.status == 200 || res.status ==201){
        document.getElementById('signin-passtoggle').removeEventListener('click', togglepass);
        document.getElementById('reg-passtoggle').removeEventListener('click', togglepass);
        signbtn.removeEventListener('click', toggleforms);
        regbtn.removeEventListener('click', toggleforms);
        regform.removeEventListener('submit', handleforms);
        signform.removeEventListener('submit', handleforms);
        socInit();
      }
    }).catch((err)=>{console.log(err)});
  }
  regform.addEventListener('submit', handleforms);
  signform.addEventListener('submit', handleforms);
}else{
  socInit();
}
/*
if (!localStorage.getItem('name')) {
  document.getElementById('sendername').addEventListener('submit', function(){
    localStorage.setItem('name', document.getElementById('name').value);
    socInit();
  })
}else{
  socInit();
}*/

/*
function crypt(str) {
 var dect={"أ":"H","ا":"h","إ":"Y","ب":"f","ت":"j","ث":"e","ج":"[","ح":"p","خ":"o","د":"]","ذ":"Q","ر":"v","ز":".","س":"s","ش":"a","ص":"w","ض":"q","ط":":","ظ":"Z","ع":"u","غ":"y","ف":"t","ق":"r","ك":";","ل":"g","م":"l","ن":"k","ه":"i","و":",","ي":"d","ئ":"z","ؤ":"c","ّ":"F",H:"أ",h:"ا",Y:"إ",f:"ب",j:"ت",e:"ث","[":"ج",p:"ح",o:"خ","]":"د",Q:"ذ",v:"ر",".":"ز",s:"س",a:"ش",w:"ص",q:"ض",":":"ط",Z:"ظ",u:"ع",y:"غ",t:"ف",r:"ق",";":"ك",g:"ل",l:"م",k:"ن",i:"ه",",":"و",d:"ي",z:"ئ",c:"ؤ",F:"ّ"};
  var converted = '';
  for (let i = 0; i < str.length; i++) {
    converted+= dect[str[i]]!=undefined?dect[str[i]]:str[i];
  }
  return converted;
}*/
function crypt(str) {
  return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
      (match, p1) => String.fromCharCode('0x' + p1)));
}
function decrypt(str) {
  return decodeURIComponent(atob(str).split('').map((c) =>
    '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
  ).join(''));
}
var cache;
function socInit() {
  const username = JSON.parse(getCookie('data')).un;
  showcase.style.display='none';
  const socket = io();

  cache = new Cache(30000);

  // if(!!localStorage.getItem('init')){
  //   socket.emit('ord',crypt(localStorage.getItem('init')));
  // }
  const messageInput = document.getElementById('message')
  var sendform = document.getElementById('sender'),
      chatwindow = document.getElementById("conv"),
      messages = ['',''];

  function sendmsg(){ 
    if(messageInput.value != ''){
      var message = messageInput.value;
          if (message == messages[0] && message == messages[1]) {
            alert('SPAM DETECTED');
          }else if(message.length > 4000){
            alert('SPAM DETECTED: TOO LONG MESSAGE');
          }
          else{
            messages[0] = messages[1];
            messages[1] = message;
            const activechannel = document.getElementsByClassName('listactivechannel')[0].dataset.cid;
            const regex = /^!\/(\w+)(?:[\s](\w+))?/g;
            if(!regex.test(message)){
              socket.emit('chat',{
                sender: crypt(JSON.parse(getCookie('data')).name),
                message: crypt(message),
                dir: crypt(messageInput.style.direction),
                channel: activechannel
              })
            }else{
              socket.emit('ord', {
                sender: crypt(JSON.parse(getCookie('data')).name),
                message: crypt(message),
                channel: activechannel
              })
            }
            socket.emit('typing',{typing: false,channel: activechannel});
          }
        messageInput.value = '';
      }  
  }
  sendform.addEventListener('submit', sendmsg);

    //tx[i].setAttribute('style', 'height:' + (tx[i].scrollHeight) + 'px;overflow-y:hidden;');
  messageInput.addEventListener("input", OnInput, false);
  
  function OnInput() {
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
    if (this.clientHeight>sendform.clientHeight) {
      this.style.alignSelf = 'flex-end';
    }else{
      this.style.alignSelf = 'unset';
    }
  }
  messageInput.addEventListener('keydown', (e)=>{
    if(e.keyCode == 13 && !e.shiftKey){
        e.preventDefault();
        sendmsg();
        OnInput.apply(messageInput);
    }
    setTimeout(()=>{ 
      if(messageInput.value.length == 1)
      {
          const x =  new RegExp("[\x00-\x80]+"); // is ascii
          const isAscii = x.test(messageInput.value);
          if(isAscii)
          {
              messageInput.style.direction= "ltr";
              messageInput.style.textAlign = "left";
          }
          else
          {
              messageInput.style.direction= "rtl";
              messageInput.style.textAlign = "right";
          }
      }},0);
    })
    messageInput.addEventListener('keyup', function(e){
      const len = messageInput.value.length;
      const ignoredkeys = ['Backspace','Shift', 'Enter','Control', 'Alt', 'Escape', 'AudioVolumeUp', 'AudioVolumeDown']
      if ((len==1||(len%3) == 0) && ignoredkeys.indexOf(e.key) == -1) {
        const activechannel = document.getElementsByClassName('listactivechannel')[0].dataset.cid;
        socket.emit('typing',{typing: true,sender: crypt(JSON.parse(getCookie('data')).name), channel: activechannel});
      }else if(e.key == 'Backspace' && (messageInput.value.length)-1<=0){
        const activechannel = document.getElementsByClassName('listactivechannel')[0].dataset.cid;
          socket.emit('typing',{typing: false,channel: activechannel});
      } 
    })
    socket.on('connect', function(){
        console.log('connected with socket');
    })
    document.getElementById('joinchannel').addEventListener('submit', e=>{
      e.preventDefault();
      socket.emit('joinroom', {id: document.getElementById('channelidinput').value});
    })
    const md = markdownit({
      linkify: true,
      typographer: true,
      breaks: true
    }).use(markdownitSup);
    function showMessage(message, parent) {
        var e = document.createElement("li");
        e.style.direction = (crypt(message.dir) == 'rtl')? 'rtl' : 'ltr';
        e.style.textAlign = (crypt(message.dir) == 'rtl')? 'right' : 'left';
          message.message = decrypt(message.message).replace(/&gt;/g, '>');
        e.innerHTML = `<span class='sender'>  ${decrypt(message.sender)} :</span>${md.render(message.message)}`;
        if(message.un == username){ 
          e.classList.add("fromyou") 
        }else{
          e.classList.add("fromanother");
        }
        parent.appendChild(e);
    }
    function showSysMessage(message, event) {
      event.type = (event.type || event.type === 0)?event.type:1;
      const chatul =  document.getElementById(message.channel);
      const s = (chatul.scrollHeight == chatul.scrollTop+chatul.offsetHeight)?true:false;
      var e = document.createElement("li");
      e.classList.add('sys-message')
      if (event.type == 1) {
        e.innerText = `  @${message.un} ${event.name} the channel`;
      }
      else if (event.type == 2) {
        e.innerText = `  @${message.un} ${event.name} @${message.toUn}`;
      }else if (event.type == 0) {
        e.innerText = event.name;
      }
      chatul.appendChild(e);
      if(s==true){
        chatul.scrollTop = chatul.scrollHeight;
      }
  }
    socket.on('leave', (data)=>{
      showSysMessage(data, {name: 'left'});
      cache.flush(data.channel);
    })
    function leave() {
      if (confirm(`You are about to leave #${this.dataset.cid}\n`)) {
        socket.emit('leave', {cid: this.dataset.cid});
      } 
    }
    document.getElementById('leavechannel').addEventListener('click', leave, false);
    socket.on('lockChange', (data)=>{
      showSysMessage(data, {name: (data.locked)? 'locked' : 'unlocked'});
    })
    socket.on('adminChange', (data)=>{
      switch (data.type) {
        case 'added':
          showSysMessage(data, {name:`made @${data.toUn} an admin in`});
          break;
        case 'removed':
          showSysMessage(data, {name: `@${data.toUn} is no longer an admin`, type: 0});
          break;
        case 'no change':
          showSysMessage(data, {name: 'They Already Is', type: 0});
        default:
          break;
      }
      
    })
    socket.on('removed', (data)=>{
      showSysMessage(data, {name: 'removed', type: 2});
      cache.flush(data.channel);
    })
    socket.on('clearChat', (data = {})=>{
      if(data.channel){
        document.getElementById(data.channel).innerHTML = '';
      }else{
        const convs = document.getElementsByClassName('convlist');
        for (let i = 0; i < convs.length; i++) {
          convs[i].innerHTML = '';
        }
      }
    })
    socket.on('ordErr', data=>{
      const err = (data.err) ? data.err : 'invalid command arguments';
        if (confirm(`Error: ${err}, do you want to send it as a normal message instead?`)) {
          socket.emit('chat', data);
        }
    })
    socket.on('init', function(data) {
      console.log(data);
      const channelslist = document.getElementById('channelslist');
      channelslist.innerHTML = '';
      chatwindow.innerHTML = '';
      if (data.length == 0) {
        const noChannelsDiv = document.createElement('div');
        noChannelsDiv.innerHTML = 
        `
        <h2>You currently don't have any joined channels</h2>
        <p>
          You Can join a channel or create a new one by pressing on the 
          <i class="fas fa-plus"></i> in the channels list
        </p>
        `;
        noChannelsDiv.classList.add('noChannels');
        chatwindow.appendChild(noChannelsDiv);
        return;
      }
      let channelslistfragment = new DocumentFragment();
      for (let i = 0; i < data.length; i++) {
        const room = data[i];
        const li = document.createElement('li');
        li.className = "channel";
        li.dataset.cid = room.id;
        const channeldiv = document.createElement('div');
        const h4 = document.createElement('h4');
        h4.innerText = room.name;
        
        const span = document.createElement('span');
        span.innerText = '#'+room.id;

        const iElem = document.createElement('i');
        iElem.className = 'fas fa-users-cog channelmembersshow';
        //iElem.dataset.cid = room.id;     
        iElem.addEventListener('click',(e)=>{
          console.log(room.id);
          hideAll(sideSections);
          membersdiv.style.display = 'block';
          sidemenu.style.display='block';
          document.getElementById('leavechannel').dataset.cid = room.id;
          cache.get(room.id, '/channeldata', {cid: room.id}).then(channeldata=>{
            console.log(channeldata);
            if (channeldata) {
              channelmembers = Object.keys(channeldata);
              let memberslistFragment = new DocumentFragment();
              document.getElementById('members-num').innerHTML = '&#9881; '+channelmembers.length;
              channelmembers.forEach(member=>{
                const li = document.createElement('li');
                li.innerText = `@${member}`;
                memberslistFragment.appendChild(li);
              })
              memberslist.innerHTML = '';
              memberslist.appendChild(memberslistFragment);
              
            }else{alert('Room data was not found, make sure you are connected to the network and refresh the page')}
          })
          
          
        })
        channeldiv.appendChild(h4)
        channeldiv.appendChild(span);
        channeldiv.appendChild(iElem)
        li.appendChild(channeldiv);
        if(i==0) li.classList.add('listactivechannel');
        li.addEventListener('click', setActiveChannel, true);
        channelslistfragment.appendChild(li);
        const div = document.createElement('div');
        div.id = room.id + 'wrapper';
        div.classList.add('convwrapper');
        if(i==0) div.classList.add('convactivechannel');
        const ul = document.createElement('ul');
        ul.className = "convlist";
        ul.id = room.id;
        room.messages.forEach(message =>{
          showMessage(message, ul);
        })
        
        let feedbox = document.createElement('div');
        feedbox.id = room.id+'feed';
        feedbox.classList.add('feedback');
        div.appendChild(ul);
        div.appendChild(feedbox);
        chatwindow.appendChild(div);
        ul.scrollTop = ul.scrollHeight;
      }
      channelslist.appendChild(channelslistfragment);
      // var s = (chatwindow.scrollHeight == chatwindow.scrollTop+chatwindow.offsetHeight)?true:chatwindow.scrollTop;
      // chatwindow.innerHTML = '';
      // data.messages.forEach(message=>{
      //   showMessage(message);        
      // })
      
      // if(s===true){
      //       chatwindow.scrollTop = chatwindow.scrollHeight;
      // }else{
      //   chatwindow.scrollTop = s;
      // }
    })
    
    function setActiveChannel(e) {
      const channels = document.getElementsByClassName('listactivechannel');
      const channelsconv = document.getElementsByClassName('convactivechannel');
      const activechannel = channels[0].dataset.cid;
      socket.emit('typing',{typing: false,channel: activechannel});
      for (let i = 0; i < channels.length; i++) {
        const channel = channels[i];
        channel.classList.remove('listactivechannel');
      }
      for (let i = 0; i < channelsconv.length; i++) {
        const channel = channelsconv[i];
        channel.classList.remove('convactivechannel');
      }
      
      e.currentTarget.classList.add('listactivechannel');
       document.getElementById(e.currentTarget.dataset.cid+'wrapper').classList.add('convactivechannel');
       sidemenu.style.display='none';
    }
    
    socket.on('chat', function(data){
      console.log(data);
      const chatul =  document.getElementById(data.channel);
      const feedback = document.getElementById(data.channel+'feed');
      if (data.message == 'ّR') {
        chatul.innerHTML = ''
      }else{
        const s = (chatul.scrollHeight == chatul.scrollTop+chatul.offsetHeight)?true:false;
        showMessage(data, chatul);
        console.log(data.channel);
        if(s==true){
            chatul.scrollTop = chatul.scrollHeight;
        }
        feedback.style.display = 'none';
      }
    })
    socket.on('join', (data)=>{
        showSysMessage(data, {name: 'joined'});
        cache.flush(data.channel);
    })
    socket.on('joinErr',(channel)=>{
      alert(`Channel #${channel} is locked, you can't join it`)
    })
    socket.on('typing', function(data){
      const feedback = document.getElementById(data.channel+'feed');
        if(data.typing == true){
            feedback.innerHTML = '<p>'+decrypt(data.sender)+' typing..</p>';
            feedback.style.display = 'flex';
        }else{
            feedback.style.display = 'none';
        }
    })
    socket.on('err', (err)=>{
      alert(err);
      clearcookies();
    })
    socket.on('reconnect', (re)=>{
      sidemenu.style.display='none';
      if (re) {
        socket.disconnect();
        socket.connect();
      }
    })
    var picker;
    var button = document.getElementById('emoji-button');
    picker = new EmojiButton({
      autoHide: false,
      showPreview: false,
      autoFocusSearch: false,
      position: 'top',
      emojiVersion: '11.0'
    });
    picker.on('emoji', function (emoji) {
      messageInput.value += emoji;
      messageInput.dispatchEvent(new Event('input'));
    });

    button.addEventListener('click', function (e) {
      picker.pickerVisible ? (picker.hidePicker(),window.setTimeout(function (){document.getElementById('message').focus()},0)) : picker.showPicker(button),document.getElementsByClassName('active')[2].getElementsByClassName('emoji-picker__emoji')[0].focus();
      e.preventDefault();
    });
}
